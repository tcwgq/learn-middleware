package com.tcwgq.zk_simple.watch;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;

import java.util.concurrent.CountDownLatch;

/**
 * @author tcwgq
 * @since 2022/7/9 20:47
 */
public class ZkConnectionWatcher implements Watcher {
    private final CountDownLatch countDownLatch;

    public ZkConnectionWatcher(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void process(WatchedEvent event) {
        if (event.getType() == Event.EventType.None) {
            if (event.getState() == Event.KeeperState.SyncConnected) {
                System.out.println("连接建立成功");
                countDownLatch.countDown();
            } else if (event.getState() == Event.KeeperState.Disconnected) {
                System.out.println("断开连接");
            } else if (event.getState() == Event.KeeperState.AuthFailed) {
                System.out.println("认证失败");
            } else if (event.getState() == Event.KeeperState.Expired) {
                System.out.println("session过期");
            }
        }
    }

}
