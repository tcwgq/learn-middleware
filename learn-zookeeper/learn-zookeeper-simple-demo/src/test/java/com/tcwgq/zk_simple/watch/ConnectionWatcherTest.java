package com.tcwgq.zk_simple.watch;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CountDownLatch;

/**
 * @author tcwgq
 * @since 2022/7/9 12:17
 */
public class ConnectionWatcherTest {
    @Test
    public void connect() throws IOException, InterruptedException, KeeperException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        ZooKeeper zk = new ZooKeeper("192.168.245.128:2181", 10 * 1000, new ZkConnectionWatcher(countDownLatch));
        countDownLatch.await();
        // 将digest改为digest1，将认证失败
        zk.addAuthInfo("digest", "tcwgq:112113".getBytes(StandardCharsets.UTF_8));
        byte[] data = zk.getData("/node2", false, null);
        System.out.println(new String(data));
        System.out.println(zk.getSessionId());
        Thread.sleep(10 * 1000);
        zk.close();
    }

}
