package com.tcwgq.zk_simple.curator;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.retry.RetryOneTime;
import org.junit.Test;

/**
 * @author tcwgq
 * @since 2022/7/10 16:18
 */
public class ConnectTest {
    @Test
    public void connect() {
        CuratorFramework client = CuratorFrameworkFactory
                .builder()
                .connectString("192.168.245.128:2181,192.168.245.129:2181,192.168.245.130:2181")
                // 会话超时时间
                .sessionTimeoutMs(5000)
                // 重试机制
                .retryPolicy(new RetryOneTime(3000))
                // 命名空间
                .namespace("create")
                .build();
        // 打开连接
        client.start();
        System.out.println(client.isStarted());
        // 关闭连接
        client.close();
    }

    @Test
    public void connect1() {
        /*
         * session重连策略
         *
         * 3秒后重连一次，只重连1次
         * RetryPolicy retryPolicy = new RetryOneTime(3000);
         *
         * 每3秒重连一次，重连3次
         * RetryPolicy retryPolicy = new RetryNTimes(3,3000);
         *
         * 每3秒重连一次，总等待时间超过10秒后停止重连
         * RetryPolicy retryPolicy=new RetryUntilElapsed(10000,3000);
         *
         * 重连3次，每次间隔时间baseSleepTimeMs * Math.max(1, random.nextInt(1 << (retryCount+ 1)))
         * RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
         */
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client = CuratorFrameworkFactory
                .builder()
                .connectString("192.168.245.128:2181,192.168.245.129:2181,192.168.245.130:2181")
                // 会话超时时间
                .sessionTimeoutMs(5000)
                // 重试机制
                .retryPolicy(retryPolicy)
                // 命名空间，即父节点
                .namespace("create")
                .build();
        // 打开连接
        client.start();
        System.out.println(client.isStarted());
        // 关闭连接
        client.close();
    }

}
