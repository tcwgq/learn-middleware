package com.tcwgq.zk_simple.distributed_lock;

import org.junit.Test;

public class MyLockTest {
    @Test
    public void lock() throws Exception {
        MyLock myLock = new MyLock();
        myLock.acquireLock();
        myLock.releaseLock();
    }

}