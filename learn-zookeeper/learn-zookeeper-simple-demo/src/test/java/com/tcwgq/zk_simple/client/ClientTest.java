package com.tcwgq.zk_simple.client;

import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * @author tcwgq
 * @since 2022/7/9 12:17
 */
public class ClientTest {
    @Test
    public void client1() throws IOException, InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        ZooKeeper zk = new ZooKeeper("192.168.245.128:2181", 10 * 1000, event -> {
            if (event.getState() == Watcher.Event.KeeperState.SyncConnected) {
                countDownLatch.countDown();
            }
        });
        countDownLatch.await();
        System.out.println(zk.getSessionId());
        zk.close();
    }

}
