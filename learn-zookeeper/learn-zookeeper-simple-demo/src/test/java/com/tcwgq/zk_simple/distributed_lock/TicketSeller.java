package com.tcwgq.zk_simple.distributed_lock;

import org.junit.Test;

public class TicketSeller {
    public static void main(String[] args) {
        TicketSeller ticketSeller = new TicketSeller();
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    ticketSeller.sellTicketWithLock();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }).start();
        }
    }

    @Test
    public void test1() throws Exception {
        for (int i = 0; i < 10; i++) {
            sellTicketWithLock();
        }
    }

    public void sellTicketWithLock() throws Exception {
        MyLock lock = new MyLock();
        // 获取锁
        lock.acquireLock();
        sell();
        // 释放锁
        lock.releaseLock();
    }

    private void sell() {
        System.out.println("售票开始");
        // 线程随机休眠数毫秒，模拟现实中的费时操作
        try {
            // 代表复杂逻辑执行了一段时间
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("售票结束");
    }
}