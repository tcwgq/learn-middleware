package com.tcwgq.rabbitmq_simple.receive;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.*;
import com.tcwgq.rabbitmq_simple.utils.ConnUtil;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 消息消费
 *
 * @author tcwgq
 * @since 2018年1月15日下午3:48:43
 */
public class ReceiveTest {
    /**
     * 发布订阅模式，消息的确认机制不是同步进行的
     *
     * @throws IOException
     * @throws TimeoutException
     */
    @Test
    public void testReceive() throws IOException, TimeoutException {
        Connection conn = ConnUtil.getConnection();
        Channel channel = conn.createChannel();
        // false表示消费者接收消息后，需要手动给broker端发送确认，一般是消费端处理完具体业务逻辑再发送确认
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, BasicProperties properties, byte[] body)
                    throws IOException {
                long deliveryTag = envelope.getDeliveryTag();
                System.out.println(new String(body));
                channel.basicAck(deliveryTag, false);
            }
        };
        String queue = "tque2";
        channel.basicConsume(queue, false, "myTag1", consumer);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ConnUtil.close(conn);
    }

    /**
     * 普通模式获取，消息同步确认
     *
     * @throws IOException
     * @throws TimeoutException
     */
    @Test
    public void testReceive1() throws IOException, TimeoutException {
        Connection conn = ConnUtil.getConnection();
        Channel channel = conn.createChannel();
        boolean autoAck = false;
        GetResponse response = channel.basicGet("dque1", autoAck);
        if (response == null) {

        } else {
            BasicProperties props = response.getProps();
            System.out.println(props);
            byte[] body = response.getBody();
            System.out.println(new String(body));
            long deliveryTag = response.getEnvelope().getDeliveryTag();
            channel.basicAck(deliveryTag, false); // acknowledge receipt of the message
        }
        ConnUtil.close(conn);
    }

}
