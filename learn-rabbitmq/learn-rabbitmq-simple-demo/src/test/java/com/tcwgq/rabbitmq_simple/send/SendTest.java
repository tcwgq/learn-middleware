package com.tcwgq.rabbitmq_simple.send;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.AMQP.Queue.DeclareOk;
import com.rabbitmq.client.*;
import com.tcwgq.rabbitmq_simple.utils.ConnUtil;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * 消息发送，* 表是匹配一个任意词组，#表示匹配0个或多个词组
 *
 * @author tcwgq
 * @since 2018年1月15日下午3:48:43
 */
public class SendTest {
    private final String exchangeName = "ex";
    private final String queueName = "que";
    private final String routingKey = "ab";

    @Test
    public void testBind1() throws IOException, TimeoutException {
        Connection conn = ConnUtil.getConnection();
        Channel channel = conn.createChannel();
        // 持久化，non-autodelete，direct类型的exchange
        channel.exchangeDeclare(exchangeName, "direct", true);
        // non-durable, exclusive,autodelete，此queue的名称由broker端自动生成
        // 排他的（只对当前client同一个Connection可用,
        // 同一个Connection的不同的Channel可共用），并且也会在client连接断开时自动删除
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, exchangeName, routingKey);
        channel.close();
        ConnUtil.close(conn);
    }

    @Test
    public void testBind2() throws IOException, TimeoutException {
        Connection conn = ConnUtil.getConnection();
        Channel channel = conn.createChannel();
        // 持久化，non-autodelete，direct类型的exchange
        channel.exchangeDeclare(exchangeName, "direct", true);
        // 这里的queue是durable的，非排他的，non-autodelete, 而且由客户端指定名称
        DeclareOk declareOk = channel.queueDeclare(queueName, true, false, false, null);
        System.out.println(declareOk.getConsumerCount());
        System.out.println(declareOk.getMessageCount());
        System.out.println(declareOk.getQueue());
        channel.queueBind(queueName, exchangeName, routingKey);
        channel.close();
        ConnUtil.close(conn);
    }

    @Test
    public void testSend() throws IOException, TimeoutException {
        Connection conn = ConnUtil.getConnection();
        Channel channel = conn.createChannel();
        // exchanges
        channel.exchangeDeclare(exchangeName, "topic", true);
        // queues
        channel.queueDeclare(queueName, true, false, false, null);
        channel.queueBind(queueName, exchangeName, "*.com.tcwgq.#");
        String msg = "Hello world, rabbitmq!";
        byte[] bytes = msg.getBytes();
        // 简单发送
        channel.basicPublish(exchangeName, "a.com.tcwgq.www", null, bytes);
        channel.basicPublish(exchangeName, "a.com.baidu.www", null, bytes);
        channel.basicPublish(exchangeName, "a.com.b", false, MessageProperties.TEXT_PLAIN, bytes);
        channel.close();
        ConnUtil.close(conn);
    }

    @Test
    public void testSend1() throws IOException, TimeoutException {
        Connection conn = ConnUtil.getConnection();
        Channel channel = conn.createChannel();
        // exchanges
        channel.exchangeDeclare(exchangeName, "direct", true);
        // queues
        channel.queueDeclare(queueName, true, false, false, null);
        channel.queueBind(queueName, exchangeName, routingKey);
        String msg = "Hello world, rabbitmq!";
        byte[] bytes = msg.getBytes();
        // 发送带附加信息的消息
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("latitude", 51.5252949);
        headers.put("longitude", -0.0905493);
        /*
         * 发送了一条消息，这条消息的delivery
         * mode为2，即消息需要被持久化在broker中，同时priority优先级为1，content-type为text/plain
         */
        channel.basicPublish("ex1", "ab", new BasicProperties.Builder().contentType("text/plain").deliveryMode(2)
                .priority(1).headers(headers).expiration("6000000").userId("tcwgq").build(), bytes);
        channel.close();
        ConnUtil.close(conn);
    }

    @Test
    public void testConfirmListener() throws IOException, TimeoutException, InterruptedException {
        Connection conn = ConnUtil.getConnection();
        Channel channel = conn.createChannel();
        // 将信道设置成confirm模式
        channel.confirmSelect();
        // exchanges
        channel.exchangeDeclare(exchangeName, "direct", true);
        // queues
        channel.queueDeclare(queueName, true, false, false, null);
        channel.queueBind(queueName, exchangeName, routingKey);
        for (int i = 1; i <= 10; i++) {
            channel.basicPublish(exchangeName, "ab", false, MessageProperties.TEXT_PLAIN, ("Hello " + i).getBytes());
            // 每条confirm一次
            // if (channel.waitForConfirms()) {
            // System.out.println("消息发送成功！");
            // }
        }
        // 批量confirm一次
        // channel.waitForConfirmsOrDie();
        // broker端的确认
        channel.addConfirmListener(new ConfirmListener() {
            @Override
            public void handleNack(long deliveryTag, boolean multiple) throws IOException {
                System.out.println("nack: deliveryTag = " + deliveryTag + " multiple: " + multiple);
            }

            @Override
            public void handleAck(long deliveryTag, boolean multiple) throws IOException {
                System.out.println("ack: deliveryTag = " + deliveryTag + " multiple: " + multiple);
            }
        });

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        channel.close();
        ConnUtil.close(conn);
    }

    @Test
    public void testReturnListener() throws IOException, TimeoutException {
        Connection conn = ConnUtil.getConnection();
        Channel channel = conn.createChannel();
        // 将信道设置成confirm模式
        channel.confirmSelect();
        // exchanges
        channel.exchangeDeclare(exchangeName, "direct", true);
        // queues
        channel.queueDeclare(queueName, true, false, false, null);
        channel.queueBind(queueName, exchangeName, routingKey);
        String msg = "Hello world, rabbitmq!";
        byte[] bytes = msg.getBytes();
        channel.basicPublish(exchangeName, "abc", true, MessageProperties.TEXT_PLAIN, bytes);
        /*
         * 当mandatory标志位设置为true时，如果exchange根据自身类型和消息routeKey无法找到一个符合条件的queue，那么会调用basic.
         * return方法将消息返回给生产者（Basic.Return + Content-Header +
         * Content-Body）；当mandatory设置为false时，出现上述情形broker会直接将消息扔掉。
         * mandatory设置为true时，需要设置ReturnListener
         */
        channel.addReturnListener(new ReturnListener() {
            @Override
            public void handleReturn(int replyCode, String replyText, String exchange, String routingKey,
                                     BasicProperties properties, byte[] body) throws IOException {
                System.out.println("replyCode: " + replyCode + " replyText: " + replyText);
                System.out.println("exchange: " + exchange + " routingKey: " + routingKey);
                System.out.println("message: " + new String(body));

            }
        });

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        channel.close();
        ConnUtil.close(conn);
    }

    @Test
    public void testDelayQueue() throws IOException, TimeoutException {
        Connection conn = ConnUtil.getConnection();
        Channel channel = conn.createChannel();
        // exchanges
        channel.exchangeDeclare("delayEx", "direct", true);
        // 死信exchange
        channel.exchangeDeclare("dlxEx", "direct", true);
        // 延时队列
        Map<String, Object> args = new HashMap<>();
        // 设置死信过期时间，单位为毫秒
        args.put("x-message-ttl", 60000);
        // 设置dlx
        args.put("x-dead-letter-exchange", "delayEx");
        // x-dead-letter-routing-key设置消息发送到DLX所用的routing-key，如果不设置默认使用消息本身的routing-key
        args.put("x-dead-letter-routing-key", "dlxrkey");
        // queues
        channel.queueDeclare("delayQue", true, false, false, args);
        channel.queueDeclare("dlxQue", true, false, false, null);
        // binds
        channel.queueBind("delayEx", "delayQue", routingKey);
        // dlx bind
        channel.queueBind("dlxQue", "dlxEx", "dlxrkey");

        String msg = "Hello world, rabbitmq!";
        byte[] bytes = msg.getBytes();
        // 或者设置单个消息的过期时间，单位毫秒，注意当队列过期时间和消息过期时间都存在时，取两者中较短的时间
        BasicProperties properties = new BasicProperties.Builder().expiration("60000").build();
        channel.basicPublish(exchangeName, "abc", true, properties, bytes);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        channel.close();
        ConnUtil.close(conn);
    }

}
