package com.tcwgq.rabbitmq_simple.send;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;
import com.tcwgq.rabbitmq_simple.utils.ConnUtil;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * 消息发送，* 表是匹配一个任意词组，#表示匹配0个或多个词组
 *
 * @author tcwgq
 * @since 2018年1月15日下午3:48:43
 */
public class SendTest1 {
    @Test
    public void testDirect() throws IOException, TimeoutException {
        Connection conn = ConnUtil.getConnection();
        Channel channel = conn.createChannel();
        // exchanges
        channel.exchangeDeclare("dex1", "direct", true);
        channel.exchangeDeclare("dex2", "direct", true);
        // queues
        channel.queueDeclare("dque1", true, false, false, null);
        channel.queueDeclare("dque2", true, false, false, null);
        channel.queueDeclare("dque3", true, false, false, null);
        // bind
        channel.queueBind("dque1", "dex1", "ab");
        channel.queueBind("dque2", "dex1", "a");
        channel.queueBind("dque3", "dex2", "abc");
        String msg = "Hello world, rabbitmq!";
        // message
        byte[] bytes = msg.getBytes();
        channel.basicPublish("dex1", "ab", true, MessageProperties.TEXT_PLAIN, bytes);
        // 发送带附加信息的消息
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("latitude", 51.5252949);
        headers.put("longitude", -0.0905493);
        channel.basicPublish("dex2", "ab", new AMQP.BasicProperties.Builder().contentType("text/plain").deliveryMode(2)
                .priority(1).headers(headers).expiration("6000000").userId("tcwgq").build(), bytes);
        channel.close();
        ConnUtil.close(conn);
    }

    @Test
    public void testFanout() throws IOException, TimeoutException {
        Connection conn = ConnUtil.getConnection();
        Channel channel = conn.createChannel();
        // exchanges
        channel.exchangeDeclare("fex1", "fanout", true);
        channel.exchangeDeclare("fex2", "fanout", true);
        // queues
        channel.queueDeclare("fque1", true, false, false, null);
        channel.queueDeclare("fque2", true, false, false, null);
        channel.queueDeclare("fque3", true, false, false, null);
        // bind
        channel.queueBind("fque1", "fex1", "a");
        channel.queueBind("fque2", "fex1", "b");
        channel.queueBind("fque3", "fex2", "c");
        String msg = "Hello world, rabbitmq!";
        // message
        byte[] bytes = msg.getBytes();
        channel.basicPublish("fex1", "1", true, MessageProperties.TEXT_PLAIN, bytes);
        // 发送带附加信息的消息
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("latitude", 51.5252949);
        headers.put("longitude", -0.0905493);
        channel.basicPublish("fex2", "2", new AMQP.BasicProperties.Builder().contentType("text/plain").deliveryMode(2)
                .priority(1).headers(headers).expiration("6000000").userId("tcwgq").build(), bytes);
        channel.close();
        ConnUtil.close(conn);
    }

    @Test
    public void testTopic() throws IOException, TimeoutException {
        Connection conn = ConnUtil.getConnection();
        Channel channel = conn.createChannel();
        // exchanges
        channel.exchangeDeclare("tex1", "topic", true);
        channel.exchangeDeclare("tex2", "topic", true);
        // queues
        channel.queueDeclare("tque1", true, false, false, null);
        channel.queueDeclare("tque2", true, false, false, null);
        channel.queueDeclare("tque3", true, false, false, null);
        // bind
        channel.queueBind("tque1", "tex1", "#.com.tcwgq.*");
        channel.queueBind("tque2", "tex1", "*.com.wgq.#");
        channel.queueBind("tque3", "tex2", "#.com.*");
        String msg = "Hello world, rabbitmq!";
        // message
        byte[] bytes = msg.getBytes();
        channel.basicPublish("tex1", "a.com.wgq.b", true, MessageProperties.TEXT_PLAIN, bytes);
        // 发送带附加信息的消息
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("latitude", 51.5252949);
        headers.put("longitude", -0.0905493);
        channel.basicPublish("tex2", "a.com.baidu.www", new AMQP.BasicProperties.Builder().contentType("text/plain")
                .deliveryMode(2).priority(1).headers(headers).expiration("6000000").userId("tcwgq").build(), bytes);
        channel.close();
        ConnUtil.close(conn);
    }

}
