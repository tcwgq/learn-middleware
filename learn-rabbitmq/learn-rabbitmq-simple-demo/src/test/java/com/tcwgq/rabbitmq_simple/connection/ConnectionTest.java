package com.tcwgq.rabbitmq_simple.connection;

import com.rabbitmq.client.*;
import com.rabbitmq.client.impl.DefaultExceptionHandler;
import com.rabbitmq.client.impl.nio.NioParams;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

/**
 * 获取连接的两种方式
 *
 * @author tcwgq
 * @since 2018年1月15日下午3:48:43
 */
public class ConnectionTest {
    @Test
    public void testConn() throws IOException, TimeoutException {
        ExecutorService service = Executors.newFixedThreadPool(10);
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("tcwgq");
        factory.setPassword("112113");
        factory.setHost("localhost");
        factory.setPort(5672);
        factory.setVirtualHost("/");
        // 使用NIO
        factory.useNio();
        // 设置NIO参数
        factory.setNioParams(new NioParams().setNbIoThreads(4));
        // 设置自动恢复，默认为true
        factory.setAutomaticRecoveryEnabled(true);
        // 关闭拓扑自动恢复
        factory.setTopologyRecoveryEnabled(false);
        // 网络重试间隔为10s
        factory.setNetworkRecoveryInterval(10000);
        // 添加恢复时的监听器
        ((Recoverable) factory).addRecoveryListener(new RecoveryListener() {
            @Override
            public void handleRecoveryStarted(Recoverable recoverable) {

            }

            @Override
            public void handleRecovery(Recoverable recoverable) {

            }
        });
        /*
         * 设置异常处理器，在connection, channel, recovery, consumer生命周期内涉及的未被处理的异常可以委托给exception
         * handler
         */
        factory.setExceptionHandler(new DefaultExceptionHandler());
        /**
         * 当connection关闭的时候，默认的ExecutorService会被shutdown,
         * 但是如果是自定义的ExecutorService将不会被自动的shutdown，所以Clients程序需要在最终关闭的时候手动的去执行shutdown()，否则将会阻止JVM的正常关闭。
         */
        // 使用address
        Address[] addresses = new Address[]{new Address("localhost"), new Address("localhost")};
        // 使用AddressResolver
        DnsRecordIpAddressResolver resolver = new DnsRecordIpAddressResolver();
        Connection connection = factory.newConnection(service, addresses);
        connection.addShutdownListener(new ShutdownListener() {
            @Override
            public void shutdownCompleted(ShutdownSignalException cause) {

            }
        });
        connection.close();
    }

    @Test
    public void testConn1()
            throws IOException, TimeoutException, KeyManagementException, NoSuchAlgorithmException, URISyntaxException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqp://tcwgq:112113@localhost:5672/");
        Connection connection = factory.newConnection();
        connection.close();
    }

}
