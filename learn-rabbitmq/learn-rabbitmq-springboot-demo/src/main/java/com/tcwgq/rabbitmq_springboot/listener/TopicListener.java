package com.tcwgq.rabbitmq_springboot.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author tcwgq
 * @since 2022/7/22 19:29
 */
@Slf4j
@Component
public class TopicListener implements MessageListener {
    @RabbitListener(queues = "springboot-queue")
    @Override
    public void onMessage(Message message) {
        log.info("TopicListener receive message={}", message);
    }

}
