package com.tcwgq.rabbitmq_springboot.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tcwgq
 * @since 2022/7/22 19:20
 */
@Configuration
public class RabbitmqConfig {
    @Bean("queue")
    public Queue queue() {
        return QueueBuilder.durable("springboot-queue").build();
    }

    @Bean("exchange")
    public Exchange exchange() {
        return ExchangeBuilder.topicExchange("springboot-exchange").build();
    }

    @Bean
    public Binding binding(@Qualifier("queue") Queue queue, @Qualifier("exchange") Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("boot.#").noargs();
    }

}
