package com.tcwgq.rabbitmq_springboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author tcwgq
 * @since 2022/7/22 19:27
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class SendTest {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void send() {
        rabbitTemplate.convertAndSend("springboot-exchange", "boot.hello", "hello, springboot");
    }

}
