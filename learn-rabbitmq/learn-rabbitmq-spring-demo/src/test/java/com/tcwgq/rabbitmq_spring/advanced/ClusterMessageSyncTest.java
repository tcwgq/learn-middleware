package com.tcwgq.rabbitmq_spring.advanced;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 集群消息同步测试
 */
public class ClusterMessageSyncTest {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1.创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 2. 设置参数
        factory.setHost("192.168.245.22");// ip  HaProxy的ip
        factory.setPort(5673); // 端口 HaProxy的监听的端口
        // 3. 创建连接 Connection
        Connection connection = factory.newConnection();
        // 4. 创建Channel
        Channel channel = connection.createChannel();
        // 5. 创建队列Queue
        channel.queueDeclare("hello_world", true, false, false, null);
        String body = "hello rabbitmq~~~";
        // 6. 发送消息
        for (int i = 0; i < 100; i++) {
            channel.basicPublish("", "hello_world", null, body.getBytes());
        }
        // 7.释放资源
        channel.close();
        connection.close();
        System.out.println("send success....");
    }

}
