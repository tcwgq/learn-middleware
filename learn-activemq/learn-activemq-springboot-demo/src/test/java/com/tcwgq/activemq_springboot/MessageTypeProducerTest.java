package com.tcwgq.activemq_springboot;

import com.tcwgq.activemq_springboot.bean.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.*;
import java.io.File;
import java.io.FileInputStream;

/**
 * 不同类型消息发送测试
 *
 * @author tcwgq
 * @since 2022/7/25 10:14
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MessageTypeProducerTest {
    // JmsMessagingTemplate: 用于工具类发送消息
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Autowired
    private JmsTemplate jmsTemplate;

    /**
     * 发送TextMessage消息
     */
    @Test
    public void textMessage() {
        jmsTemplate.send("text_queue", new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createTextMessage("文本消息");
            }
        });
    }

    /**
     * 发送MapMessage消息
     */
    @Test
    public void mapMessage() {
        jmsTemplate.send("map_queue", new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                MapMessage mapMessage = session.createMapMessage();
                mapMessage.setString("name", "张三");
                mapMessage.setInt("age", 20);
                return mapMessage;
            }
        });
    }

    /**
     * 发送ObjectMessage消息
     */
    @Test
    public void objectMessage() {
        jmsTemplate.send("object_queue", new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                User user = new User("小明", "123456");
                return session.createObjectMessage(user);
            }
        });
    }

    /**
     * 发送BytesMessage消息
     */
    @Test
    public void bytesMessage() {
        jmsTemplate.send("byte_queue", new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                BytesMessage bytesMessage = session.createBytesMessage();
                // 1.读取文件
                File file = new File("d:/activemq/spring.jpg");
                // 2.构建文件输入流
                try {
                    FileInputStream inputStream = new FileInputStream(file);
                    // 3.把文件流写入到缓存数组中
                    byte[] buffer = new byte[(int) file.length()];
                    inputStream.read(buffer);
                    // 4.把缓存数组写入到BytesMessage中
                    bytesMessage.writeBytes(buffer);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return bytesMessage;
            }
        });
    }

    /**
     * 发送StreamMessage消息
     */
    @Test
    public void streamMessage() {
        jmsTemplate.send("stream_queue", new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                StreamMessage streamMessage = session.createStreamMessage();
                streamMessage.writeString("你好，ActiveMQ");
                streamMessage.writeInt(20);
                // 设置消息属性：标记、过滤
                streamMessage.setStringProperty("订单", "order");
                return streamMessage;
            }
        });
    }

}
