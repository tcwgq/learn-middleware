package com.tcwgq.activemq_springboot;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQMessageProducer;
import org.apache.activemq.ScheduledMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.*;

/**
 * 延时投递测试，只能使用原生api，springboot无法支持
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MessageDelayProducerTest {
    /**
     * 延迟投递
     */
    @Test
    public void sendMessage() throws JMSException {
        // 获取连接工厂
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://192.168.245.128:61616");
        Connection connection = factory.createConnection();
        Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
        Queue queue = session.createQueue("delay_queue");
        ActiveMQMessageProducer producer = (ActiveMQMessageProducer) session.createProducer(queue);
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        // 创建需要发送的消息
        TextMessage textMessage = session.createTextMessage("Hello");
        // 设置延时时长(延时10秒)
        textMessage.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, 10000);
        producer.send(textMessage);
        session.commit();
        session.close();
        connection.close();
    }

}
