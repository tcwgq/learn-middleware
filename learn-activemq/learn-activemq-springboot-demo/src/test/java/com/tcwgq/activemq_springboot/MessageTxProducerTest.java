package com.tcwgq.activemq_springboot;

import com.tcwgq.activemq_springboot.service.MessageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.*;

/**
 * 消息事务测试
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MessageTxProducerTest {
    private final String name = "tx_queue";

    // JmsMessagingTemplate: 用于工具类发送消息
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private MessageService messageService;

    /**
     * 没有加入事务
     */
    @Test
    public void testMessage() {
        for (int i = 1; i <= 10; i++) {
            // 模拟异常
            if (i == 4) {
                int a = 10 / 0;
            }
            jmsMessagingTemplate.convertAndSend(name, "消息---" + i);
        }
    }

    /**
     * 事务性发送--方案一，原生api
     */
    @Test
    public void sendMessageTx() {
        // 获取连接工厂
        ConnectionFactory connectionFactory = jmsMessagingTemplate.getConnectionFactory();
        Session session = null;
        try {
            // 创建连接
            Connection connection = connectionFactory.createConnection();
            /*
             * 参数一：是否开启消息事务
             */
            session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);

            // 创建生产者
            MessageProducer producer = session.createProducer(session.createQueue(name));

            for (int i = 1; i <= 10; i++) {
                // 模拟异常
                // if(i==4){
                //     int a = 10/0;
                // }

                TextMessage textMessage = session.createTextMessage("消息--" + i);
                producer.send(textMessage);
            }

            // 注意：一旦开启事务发送，那么就必须使用commit方法进行事务提交，否则消息无法到达MQ服务器
            session.commit();
        } catch (JMSException e) {
            e.printStackTrace();
            // 消息事务回滚
            try {
                session.rollback();
            } catch (JMSException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * 事务性发送--方案二： Spring的JmsTransactionManager功能，注意代码不能写在测试类，否则事务aop不起作用
     */
    @Test
    public void sendMessageTx2() {
        messageService.sendMessage();
    }

    @Test
    public void testMessage2() {
        jmsMessagingTemplate.convertAndSend(name, "测试消息");
    }

}
