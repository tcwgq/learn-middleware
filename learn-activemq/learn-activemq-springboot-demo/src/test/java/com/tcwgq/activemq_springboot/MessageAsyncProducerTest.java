package com.tcwgq.activemq_springboot;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQMessageProducer;
import org.apache.activemq.AsyncCallback;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.jms.*;

/**
 * 消息异步投递测试
 * <p>
 * 想要使用异步，在brokerURL中增加 jms.alwaysSyncSend=false&jms.useAsyncSend=true 属性
 * 1）如果设置了alwaysSyncSend=true系统将会忽略useAsyncSend设置的值都采用同步
 * 2）当alwaysSyncSend=false时，“NON_PERSISTENT”(非持久化)、事务中的消息将使用“异步发送”
 * 3）当alwaysSyncSend=false时，如果指定了useAsyncSend=true，“PERSISTENT”类型的消息使用异步发送。
 * 如果useAsyncSend=false，“PERSISTENT”类型的消息使用同步发送。
 * 总结： 默认情况(alwaysSyncSend=false,useAsyncSend=false)，非持久化消息、事务内的消息均采用
 * 异步发送；对于持久化消息采用同步发送！！！
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MessageAsyncProducerTest {
    // JmsMessagingTemplate: 用于工具类发送消息
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Resource
    private JmsTemplate jmsTemplate;

    /**
     * 异步投递，方式一：原生api
     * 1.在连接上配置new ActiveMQConnectionFactory("tcp://localhost:61616?jms.useAsyncSend=true");
     * 2.通过ConnectionFactory((ActiveMQConnectionFactory)connectionFactory).setUseAsyncSend(true);
     * 3.通过connection((ActiveMQConnection)connection).setUseAsyncSend(true);
     */
    @Test
    public void sendMessage() throws JMSException {
        // 1.创建工厂
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://192.168.245.128:61616");
        factory.setUseAsyncSend(true);
        // 2.创建连接
        Connection connection = factory.createConnection();
        // 3.打开连接
        connection.start();
        // 4.创建session
        /*
         * 参数一：是否开启事务
         * 参数二：是否自动确认
         */
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        // 5.创建目标地址，queue表示点对点模式，topic对应发布订阅模式
        Queue queue = session.createQueue("async_queue");
        // 6.创建发送者
        ActiveMQMessageProducer producer = (ActiveMQMessageProducer) session.createProducer(queue);
        // 7.创建消息
        TextMessage textMessage = session.createTextMessage("Hello, async_queue!");
        String msgid = textMessage.getJMSMessageID();
        // 8.发送消息，同时注册回调，监控发小发送状态
        producer.send(textMessage, new AsyncCallback() {
            @Override
            public void onSuccess() {
                // 使用msgid标识来进行消息发送成功的处理
                System.out.println(msgid + " 消息发送成功");
            }

            @Override
            public void onException(JMSException exception) {
                // 使用msgid表示进行消息发送失败的处理
                System.out.println(msgid + " 消息发送失败");
                exception.printStackTrace();
            }
        });
        System.out.println("消息发送成功...");
        // 9.释放资源
        session.close();
        connection.close();
    }

    /**
     * 异步投递，方式二：使用jmxTemplate
     */
    @Test
    public void sendMessage1() {
        jmsTemplate.convertAndSend("async_queue", "Hello, async_queue!");
    }

}
