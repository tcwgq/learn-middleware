package com.tcwgq.activemq_springboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * springboot集成
 *
 * @author tcwgq
 * @since 2022/7/24 11:21
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ProducerTest {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Test
    public void send() {
        jmsMessagingTemplate.convertAndSend("springboot_queue", "Hello springboot!");
    }

    @Test
    public void send1() {
        jmsMessagingTemplate.convertAndSend("springboot_topic", "Hello springboot topic!");
    }

}
