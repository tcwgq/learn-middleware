package com.tcwgq.activemq_springboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 * 消息头设置测试
 * 虽然很多消息提供了设置消息头的set方法，但是即使手动设置了一些消息头，也不会起作用，还是会使用系统自动生成的
 *
 * @author tcwgq
 * @since 2022/7/25 10:14
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MessageHeaderProducerTest {
    // JmsMessagingTemplate: 用于工具类发送消息
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Autowired
    private JmsTemplate jmsTemplate;

    /**
     * 发送TextMessage消息
     */
    @Test
    public void textMessage() {
        jmsTemplate.send("header_queue", new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                TextMessage message = session.createTextMessage("文本消息");
                message.setJMSCorrelationID("10000");// 设置了起作用
                message.setJMSPriority(7); // 设置了也不起作用
                message.setJMSMessageID("20000");// 设置了也不起作用
                message.setJMSDeliveryMode(5);
                // message.setJMSDeliveryTime(System.currentTimeMillis());// 设置后报错
                message.setJMSRedelivered(true);// 设置了也不起作用
                message.setJMSType("afafas"); // 设置了起作用
                message.setJMSReplyTo(session.createQueue("replay_queue"));// 设置了起作用
                // TODO 还有哪些手动设置会起作用，自己尝试

                return message;
            }
        });
    }

}
