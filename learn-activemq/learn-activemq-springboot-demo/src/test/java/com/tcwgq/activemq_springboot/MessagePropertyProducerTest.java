package com.tcwgq.activemq_springboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 * 消息属性测试
 *
 * @author tcwgq
 * @since 2022/7/25 10:14
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MessagePropertyProducerTest {
    // JmsMessagingTemplate: 用于工具类发送消息
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Autowired
    private JmsTemplate jmsTemplate;

    /**
     * 发送TextMessage消息
     */
    @Test
    public void textMessage() {
        jmsTemplate.send("property_queue", new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                TextMessage message = session.createTextMessage("文本消息");
                message.setStringProperty("name", "zhangSan");
                message.setIntProperty("age", 24);

                return message;
            }
        });
    }

}
