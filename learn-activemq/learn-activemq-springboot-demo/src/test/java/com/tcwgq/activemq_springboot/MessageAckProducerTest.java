package com.tcwgq.activemq_springboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 消息确认机制测试
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MessageAckProducerTest {
    // JmsMessagingTemplate: 用于工具类发送消息
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Autowired
    private JmsTemplate jmsTemplate;

    @Test
    public void testMessage() {
        for (int i = 0; i < 10; i++) {
            jmsMessagingTemplate.convertAndSend("ack_queue", "消息-" + i);
        }
    }

}
