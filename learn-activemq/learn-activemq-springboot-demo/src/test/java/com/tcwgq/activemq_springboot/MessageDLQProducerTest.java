package com.tcwgq.activemq_springboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 死信队列测试
 * 死信队列，用来保存处理失败或者过期的消息
 * 出现以下情况时，消息会被重发：
 * A transacted session is used and rollback() is called.
 * A transacted session is closed before commit is called.
 * A session is using CLIENT_ACKNOWLEDGE and Session.recover() is called.
 * 当一个消息被重发超过6(缺省为6次)次数时，会给broker发送一个"Poison ack"，这个消息被认为是a
 * poison pill，这时broker会将这个消息发送到死信队列，以便后续处理。
 * 注意两点：
 * 1）缺省持久消息过期，会被送到DLQ，非持久消息不会送到DLQ
 * 2）缺省的死信队列是ActiveMQ.DLQ，如果没有特别指定，死信都会被发送到这个队列。
 * 可以通过配置文件(activemq.xml)来调整死信发送策略。
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MessageDLQProducerTest {
    // JmsMessagingTemplate: 用于工具类发送消息
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Autowired
    private JmsTemplate jmsTemplate;

    @Test
    public void testMessage() {
        jmsMessagingTemplate.convertAndSend("dead_queue", "Hello, dead queue!");
    }

}
