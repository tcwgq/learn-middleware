package com.tcwgq.activemq_springboot.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 * 产生死信队列
 */
@Component
public class DLQMsgListener {
    private int count = 1;

    /**
     * 接收TextMessage的方法
     */
    @JmsListener(destination = "dead_queue")
    public void receiveMessage(Message message, Session session) {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            try {
                System.out.println("接收消息：" + textMessage.getText());
                System.out.println(count++);
                // 手动确认
                int a = 1 / 0;
                textMessage.acknowledge();
            } catch (JMSException e) {
                e.printStackTrace();
                try {
                    session.rollback();
                } catch (JMSException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

}
