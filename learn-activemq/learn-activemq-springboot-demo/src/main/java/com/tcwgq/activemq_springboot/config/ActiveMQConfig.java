package com.tcwgq.activemq_springboot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.connection.JmsTransactionManager;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.transaction.PlatformTransactionManager;

import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;

/**
 * ActiveMQ配置类
 */
@Configuration
public class ActiveMQConfig {
    /**
     * 事务性发送--方案二： Spring的JmsTransactionManager功能
     * 添加Jms事务管理器
     */
    @Bean
    public PlatformTransactionManager createTransactionManager(ConnectionFactory connectionFactory) {
        return new JmsTransactionManager(connectionFactory);
    }

    /**
     * 在事务性会话中，当一个事务被提交的时候，确认自动发生，所以这里需要手动配置DefaultJmsListenerContainerFactory
     */
    @Bean(name = "jmsQueryListenerFactory")
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory(ConnectionFactory connectionFactory) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        // 关闭事务，事务消息会自动确认
        factory.setSessionTransacted(false);
        // 修改消息确认机制，注意：spring整合activemq后，手动确认是4，而不是Session.CLIENT_ACKNOWLEDGE
        factory.setSessionAcknowledgeMode(4);
        return factory;
    }

    /**
     * 异步投递，方式二：使用jmxTemplate
     * 配置用于异步发送的非持久化JmsTemplate
     */
    // @Bean("asyncJmsTemplate")
    public JmsTemplate asyncJmsTemplate(ConnectionFactory connectionFactory) {
        JmsTemplate template = new JmsTemplate(connectionFactory);
        template.setExplicitQosEnabled(true);
        template.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        return template;
    }

    /**
     * 配置用于同步发送的持久化JmsTemplate
     */
    // @Bean("syncJmsTemplate")
    public JmsTemplate syncJmsTemplate(ConnectionFactory connectionFactory) {
        return new JmsTemplate(connectionFactory);
    }

}
