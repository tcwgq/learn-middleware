package com.tcwgq.activemq_springboot.listener;

import com.tcwgq.activemq_springboot.bean.User;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.*;
import java.io.FileOutputStream;

/**
 * 用于监听消息类（既可以用于队列的监听，也可以用于主题监听）
 *
 * @author tcwgq
 * @since 2022/7/24 10:32
 */
@Component
public class MsgListener {
    @JmsListener(destination = "springboot_queue")
    public void queue(Message message) {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            try {
                System.out.println(textMessage.getText());
            } catch (JMSException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @JmsListener(destination = "springboot_topic")
    public void topic(Message message) {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            try {
                System.out.println(textMessage.getText());
            } catch (JMSException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 接收TextMessage的方法
     */
    @JmsListener(destination = "text_queue")
    public void text(Message message) {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            try {
                System.out.println("接收消息：" + textMessage.getText());
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }

    @JmsListener(destination = "map_queue")
    public void map(Message message) {
        if (message instanceof MapMessage) {
            MapMessage mapMessage = (MapMessage) message;
            try {
                System.out.println("名称：" + mapMessage.getString("name"));
                System.out.println("年龄：" + mapMessage.getString("age"));
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }

    @JmsListener(destination = "object_queue")
    public void object(Message message) {
        if (message instanceof ObjectMessage) {
            ObjectMessage objectMessage = (ObjectMessage) message;
            try {
                User user = (User) objectMessage.getObject();
                System.out.println(user.getUsername());
                System.out.println(user.getPassword());
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }

    @JmsListener(destination = "byte_queue")
    public void byteMessage(Message message) {
        if (message instanceof BytesMessage) {
            BytesMessage bytesMessage = (BytesMessage) message;
            try {
                System.out.println("接收消息内容：" + bytesMessage.getBodyLength());
                // 1.设计缓存数组
                byte[] buffer = new byte[(int) bytesMessage.getBodyLength()];
                // 2.把字节消息的内容写入到缓存数组
                bytesMessage.readBytes(buffer);
                // 3.构建文件输出流
                FileOutputStream outputStream = new FileOutputStream("d:/activemq/test.jpg");
                // 4.把数据写出本地硬盘
                outputStream.write(buffer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @JmsListener(destination = "stream_queue")
    public void receiveStreamMessage(Message message) {
        if (message instanceof StreamMessage) {
            StreamMessage streamMessage = (StreamMessage) message;
            try {
                // 接收消息属性
                System.out.println(streamMessage.getStringProperty("订单"));
                System.out.println(streamMessage.readString());
                System.out.println(streamMessage.readInt());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @JmsListener(destination = "property_queue")
    public void property_queue(Message message) {
        TextMessage textMessage = (TextMessage) message;
        try {
            System.out.println(textMessage.getText());
            System.out.println(textMessage.getStringProperty("name"));
            System.out.println(textMessage.getIntProperty("age"));
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    @JmsListener(destination = "header_queue")
    public void header_queue(Message message) {
        TextMessage textMessage = (TextMessage) message;
        try {
            System.out.println(textMessage.getText());
            System.out.println(textMessage.getJMSCorrelationID());
            System.out.println(textMessage.getJMSMessageID());
            System.out.println(textMessage.getJMSPriority());
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

}
