package com.tcwgq.activemq_springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author tcwgq
 * @since 2022/7/24 11:15
 */
@EnableScheduling // 开启定时功能
@SpringBootApplication
public class ActivemqApplication {
    public static void main(String[] args) {
        SpringApplication.run(ActivemqApplication.class, args);
    }

}
