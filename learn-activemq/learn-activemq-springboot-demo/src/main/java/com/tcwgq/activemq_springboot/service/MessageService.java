package com.tcwgq.activemq_springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * 消息发送的业务类
 */
@Service
public class MessageService {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Transactional // 对消息发送加入事务管理（同时也对JDBC数据库的事务生效）
    public void sendMessage() {
        for (int i = 1; i <= 10; i++) {
            // 模拟异常
            if (i == 4) {
                int a = 10 / 0;
            }

            jmsMessagingTemplate.convertAndSend("tx_queue", "消息---" + i);
        }
    }

    /**
     * 定时投递，每隔3秒定投
     */
    // @Scheduled(fixedDelay = 3000)
    public void sendQueue() {
        jmsMessagingTemplate.convertAndSend("fix_queue", "消息ID:" + UUID.randomUUID().toString().substring(0, 6));
        System.out.println("消息发送成功...");
    }

}
