package com.tcwgq.activemq_springboot.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 * 事务消息消费
 */
@Component
public class TxMsgListener {
    /**
     * 接收TextMessage的方法
     */
    @JmsListener(destination = "tx_queue")
    public void receiveMessage(Message message, Session session) {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            try {
                System.out.println("接收消息：" + textMessage.getText());
                // int i = 10 / 0;
                // 提交事务
                // session.commit(); // 事务消息会自动提交，这里不写也行
            } catch (JMSException e) {
                e.printStackTrace();
                // 回滚事务
                try {
                    session.rollback();// 一旦事务回滚，MQ会重发消息，一共重发6次
                } catch (JMSException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

}
