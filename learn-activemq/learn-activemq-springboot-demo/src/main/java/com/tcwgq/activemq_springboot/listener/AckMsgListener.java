package com.tcwgq.activemq_springboot.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 * 手动确认测试，不确认的情况下，消息会正常消费，但不会从服务端移除
 */
@Component
public class AckMsgListener {
    /**
     * 接收TextMessage的方法
     */
    @JmsListener(destination = "ack_queue", containerFactory = "jmsQueryListenerFactory")
    public void receiveMessage(Message message, Session session) {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            try {
                System.out.println("接收消息：" + textMessage.getText());
                // 手动确认
                textMessage.acknowledge();
            } catch (JMSException e) {
                e.printStackTrace();
                try {
                    session.rollback();
                } catch (JMSException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

}
