package com.tcwgq.activemq_simple.ptp;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * 点对点receive接收消息
 *
 * @author tcwgq
 * @since 2022/7/24 9:30
 */
public class Consumer {
    public static void main(String[] args) throws JMSException {
        // 1.创建连接工厂
        ConnectionFactory factory = new ActiveMQConnectionFactory("tcp://192.168.245.128:61616");
        // 2.创建连接
        Connection connection = factory.createConnection();
        // 3.打开连接
        connection.start();
        // 4.创建session
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        // 5.指定目标地址
        Queue queue = session.createQueue("ptp_queue");
        // 6.创建消息的消费者
        MessageConsumer consumer = session.createConsumer(queue);
        // 7.接收消息
        while (true) {
            Message message = consumer.receive(100);
            // 没有消息，结束
            if (message == null) {
                break;
            }
            // 判断消息类型
            if (message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                System.out.println(textMessage.getText());
            }
        }
    }

}
