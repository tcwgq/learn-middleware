package com.tcwgq.activemq_simple.ptp;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * 点对点监听器接收消息，推荐使用
 *
 * @author tcwgq
 * @since 2022/7/24 9:30
 */
public class Consumer1 {
    public static void main(String[] args) throws JMSException {
        // 1.创建连接工厂
        ConnectionFactory factory = new ActiveMQConnectionFactory("tcp://192.168.245.128:61616");
        // 2.创建连接
        Connection connection = factory.createConnection();
        // 3.打开连接
        connection.start();
        // 4.创建session
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        // 5.指定目标地址
        Queue queue = session.createQueue("ptp_queue");
        // 6.创建消息的消费者
        MessageConsumer consumer = session.createConsumer(queue);
        // 7.配置消息监听器
        consumer.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                // 判断消息类型
                if (message instanceof TextMessage) {
                    TextMessage textMessage = (TextMessage) message;
                    try {
                        System.out.println(textMessage.getText());
                    } catch (JMSException e) {
                    }
                }
            }
        });
        // 8.不要关闭连接
    }

}
