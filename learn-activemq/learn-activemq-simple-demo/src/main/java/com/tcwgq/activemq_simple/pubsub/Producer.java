package com.tcwgq.activemq_simple.pubsub;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * 发布订阅模式，注意，有时间先后顺序，一定要让消费者先订阅topic，再发送消息才能正确消费
 *
 * @author tcwgq
 * @since 2022/7/24 9:43
 */
public class Producer {
    public static void main(String[] args) throws JMSException {
        // 1.创建工厂
        ConnectionFactory factory = new ActiveMQConnectionFactory("tcp://192.168.245.128:61616");
        // 2.创建连接
        Connection connection = factory.createConnection();
        // 3.打开连接
        connection.start();
        // 4.创建session
        /*
         * 参数一：是否开启事务
         * 参数二：是否自动确认
         */
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        // 5.创建目标地址，queue表示点对点模式，topic对应发布订阅模式
        Topic topic = session.createTopic("pubsub_topic");
        // 6.创建发送者
        MessageProducer producer = session.createProducer(topic);
        // 7.创建消息
        TextMessage textMessage = session.createTextMessage("Hello, activemq topic!");
        // 8.发送消息
        producer.send(textMessage);
        System.out.println("消息发送成功...");
        // 9.释放资源
        session.close();
        connection.close();
    }
}
