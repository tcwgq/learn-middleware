package com.tcwgq.activemq_simple.ptp;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * 点对点模式，生产消费没有时间先后顺序
 *
 * @author tcwgq
 * @since 2022/7/24 9:14
 */
public class Producer {
    public static void main(String[] args) throws JMSException {
        // 1.创建工厂
        ConnectionFactory factory = new ActiveMQConnectionFactory("tcp://192.168.245.128:61616");
        // 2.创建连接
        Connection connection = factory.createConnection();
        // 3.打开连接
        connection.start();
        // 4.创建session
        /*
         * 参数一：是否开启事务
         * 参数二：是否自动确认
         */
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        // 5.创建目标地址，queue表示点对点模式，topic对应发布订阅模式
        Queue queue = session.createQueue("ptp_queue");
        // 6.创建发送者
        MessageProducer producer = session.createProducer(queue);
        // 7.创建消息
        TextMessage textMessage = session.createTextMessage("Hello, activemq!");
        // 8.发送消息
        producer.send(textMessage);
        System.out.println("消息发送成功...");
        // 9.释放资源
        session.close();
        connection.close();
    }

}
