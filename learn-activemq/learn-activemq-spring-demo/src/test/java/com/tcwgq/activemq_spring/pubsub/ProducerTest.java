package com.tcwgq.activemq_spring.pubsub;

import com.tcwgq.activemq_spring.config.ActivemqConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

/**
 * 发布订阅模式发送消息
 *
 * @author tcwgq
 * @since 2022/7/24 10:10
 */
@ContextConfiguration(classes = ActivemqConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ProducerTest {
    @Resource(name = "topic-template")
    private JmsTemplate jmsTemplate;

    @Test
    public void pubsub() {
        jmsTemplate.send("spring_topic", new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createTextMessage("hello, spring topic!");
            }
        });
    }

}
