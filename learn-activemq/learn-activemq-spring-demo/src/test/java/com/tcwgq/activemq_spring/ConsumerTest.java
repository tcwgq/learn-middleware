package com.tcwgq.activemq_spring;

import com.tcwgq.activemq_spring.config.ActivemqConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * 点对点模式发送
 *
 * @author tcwgq
 * @since 2022/7/24 10:10
 */
@ContextConfiguration(classes = ActivemqConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ConsumerTest {
    @Test
    public void consumer() throws IOException {
        System.in.read();
    }

}
