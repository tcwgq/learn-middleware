package com.tcwgq.activemq_spring.ptp;

import com.tcwgq.activemq_spring.config.ActivemqConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

/**
 * 点对点模式发送
 *
 * @author tcwgq
 * @since 2022/7/24 10:10
 */
@ContextConfiguration(classes = ActivemqConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ProducerTest {
    @Resource(name = "ptp-template")
    private JmsTemplate jmsTemplate;

    @Test
    public void ptp() {
        jmsTemplate.send("spring_queue", new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createTextMessage("hello, spring ptp!");
            }
        });
    }

}
