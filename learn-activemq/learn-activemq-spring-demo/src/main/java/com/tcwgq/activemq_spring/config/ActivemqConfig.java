package com.tcwgq.activemq_spring.config;

import com.tcwgq.activemq_spring.listener.QueueListener;
import com.tcwgq.activemq_spring.listener.TopicListener;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import javax.jms.ConnectionFactory;

/**
 * @author tcwgq
 * @since 2022/7/24 10:00
 */
@Configuration
@ComponentScan(basePackages = "com.tcwgq.activemq_spring")
public class ActivemqConfig {
    @Bean
    public ConnectionFactory connectionFactory() {
        return new ActiveMQConnectionFactory("tcp://192.168.245.128:61616");
    }

    @Bean
    public CachingConnectionFactory cachingConnectionFactory(ConnectionFactory connectionFactory) {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory();
        cachingConnectionFactory.setSessionCacheSize(16);
        // cachingConnectionFactory.setCacheProducers();
        // cachingConnectionFactory.setCacheConsumers();
        cachingConnectionFactory.setTargetConnectionFactory(connectionFactory);
        // cachingConnectionFactory.setExceptionListener();
        // cachingConnectionFactory.setReconnectOnException();
        return cachingConnectionFactory;
    }

    @Bean("ptp-template")
    public JmsTemplate ptp(CachingConnectionFactory cachingConnectionFactory) {
        JmsTemplate jmsTemplate = new JmsTemplate();
        // jmsTemplate.setDefaultDestination();
        // jmsTemplate.setDefaultDestinationName();
        // jmsTemplate.setMessageConverter();
        jmsTemplate.setMessageIdEnabled(true);
        jmsTemplate.setMessageTimestampEnabled(true);
        // jmsTemplate.setPubSubNoLocal();
        jmsTemplate.setReceiveTimeout(10000);
        // jmsTemplate.setDeliveryDelay();
        // jmsTemplate.setExplicitQosEnabled();
        // jmsTemplate.setQosSettings();
        // jmsTemplate.setDeliveryPersistent();
        // jmsTemplate.setDeliveryMode();
        // jmsTemplate.setPriority();
        // jmsTemplate.setTimeToLive();
        // jmsTemplate.setDestinationResolver();
        // false点对点模式，true发布订阅模式
        jmsTemplate.setPubSubDomain(false);
        jmsTemplate.setConnectionFactory(cachingConnectionFactory);
        // jmsTemplate.setSessionTransacted();
        // jmsTemplate.setSessionAcknowledgeModeName();
        // jmsTemplate.setSessionAcknowledgeMode();
        return jmsTemplate;
    }

    @Bean("topic-template")
    public JmsTemplate topic(CachingConnectionFactory cachingConnectionFactory) {
        JmsTemplate jmsTemplate = new JmsTemplate();
        // jmsTemplate.setDefaultDestination();
        // jmsTemplate.setDefaultDestinationName();
        // jmsTemplate.setMessageConverter();
        jmsTemplate.setMessageIdEnabled(true);
        jmsTemplate.setMessageTimestampEnabled(true);
        // jmsTemplate.setPubSubNoLocal();
        jmsTemplate.setReceiveTimeout(10000);
        // jmsTemplate.setDeliveryDelay();
        // jmsTemplate.setExplicitQosEnabled();
        // jmsTemplate.setQosSettings();
        // jmsTemplate.setDeliveryPersistent();
        // jmsTemplate.setDeliveryMode();
        // jmsTemplate.setPriority();
        // jmsTemplate.setTimeToLive();
        // jmsTemplate.setDestinationResolver();
        // false点对点模式，true发布订阅模式
        jmsTemplate.setPubSubDomain(true);
        jmsTemplate.setConnectionFactory(cachingConnectionFactory);
        // jmsTemplate.setSessionTransacted();
        // jmsTemplate.setSessionAcknowledgeModeName();
        // jmsTemplate.setSessionAcknowledgeMode();
        return jmsTemplate;
    }

    @Bean("ptpContainer")
    public DefaultMessageListenerContainer ptpContainer(CachingConnectionFactory cachingConnectionFactory,
                                                        QueueListener queueListener) {
        DefaultMessageListenerContainer simpleMessageListenerContainer = new DefaultMessageListenerContainer();
        simpleMessageListenerContainer.setDestinationName("spring_queue");
        simpleMessageListenerContainer.setMessageListener(queueListener);
        // 点对点模式
        simpleMessageListenerContainer.setPubSubDomain(false);
        // simpleMessageListenerContainer.setReplyPubSubDomain(false);
        simpleMessageListenerContainer.setConnectionFactory(cachingConnectionFactory);
        return simpleMessageListenerContainer;
    }

    @Bean("topicContainer")
    public DefaultMessageListenerContainer topicContainer(CachingConnectionFactory cachingConnectionFactory,
                                                          TopicListener topicListener) {
        DefaultMessageListenerContainer simpleMessageListenerContainer = new DefaultMessageListenerContainer();
        simpleMessageListenerContainer.setDestinationName("spring_topic");
        simpleMessageListenerContainer.setMessageListener(topicListener);
        // 发布订阅模式
        simpleMessageListenerContainer.setPubSubDomain(true);
        // simpleMessageListenerContainer.setReplyPubSubDomain(true);
        simpleMessageListenerContainer.setConnectionFactory(cachingConnectionFactory);
        return simpleMessageListenerContainer;
    }

}
