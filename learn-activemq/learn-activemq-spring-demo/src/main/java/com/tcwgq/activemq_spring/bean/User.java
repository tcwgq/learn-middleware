package com.tcwgq.activemq_spring.bean;

import lombok.Data;

/**
 * @author tcwgq
 * @since 2022/7/24 10:24
 */
@Data
public class User {
    private Long id;
    private String username;
    private Integer age;

}
