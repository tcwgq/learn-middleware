package com.tcwgq.learn_jedis.masterslave;

import org.junit.Test;
import redis.clients.jedis.Jedis;

public class JedisTest {
    @Test
    public void testMasterSlave() {
        Jedis master = new Jedis("192.168.218.129", 6379);
        master.auth("123456");
        master.set("name", "zhangSan");
        master.close();
    }

    @Test
    public void testMasterSlave1() {
        Jedis slave = new Jedis("192.168.218.129", 6380);
        slave.auth("123456");
        System.out.println(slave.get("name"));
        System.out.println(slave.lrange("list", 0, -1));
        slave.close();
    }

    @Test
    public void testMasterSlave2() {
        Jedis slave = new Jedis("192.168.218.129", 6381);
        slave.auth("123456");
        System.out.println(slave.get("name"));
        slave.close();
    }

    @Test
    public void testMasterSlave3() {
        Jedis slave = new Jedis("192.168.218.129", 6382);
        slave.auth("123456");
        System.out.println(slave.get("name"));
        slave.close();
    }

    @Test
    public void testMasterSlave4() {
        Jedis slave = new Jedis("192.168.218.129", 6382);
        slave.auth("123456");
        System.out.println(slave.get("name"));
        slave.close();
    }
}
