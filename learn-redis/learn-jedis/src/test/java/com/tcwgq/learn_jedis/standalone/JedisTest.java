package com.tcwgq.learn_jedis.standalone;

import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashMap;
import java.util.Map;

public class JedisTest {
    @Test
    public void test1() {
        Jedis jedis = new Jedis("localhost", 6379);
        // 使用第1个数据库
        jedis.select(0);
        jedis.set("first:name", "zhangSan");
        jedis.set("second:name", "zhangSan");
        jedis.close();
    }

    @Test
    public void test2() {
        Jedis jedis = new Jedis("localhost", 6379);
        // 使用第1个数据库
        jedis.select(0);
        Long hset = jedis.hset("hello_world", "age", "27");
        System.out.println(hset);
        jedis.close();
    }

    @Test
    public void test3() {
        Jedis jedis = new Jedis("localhost", 6379);
        // 使用第1个数据库
        jedis.select(0);
        Map<String, String> map = new HashMap<>();
        map.put("zhangSan", "female");
        map.put("liSi", "male");
        String hmset = jedis.hmset("hmset", map);
        System.out.println(hmset);
        jedis.close();
    }

    @Test
    public void test4() {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxIdle(16);
        config.setMaxTotal(128);
        config.setMaxWaitMillis(6000);
        config.setTestOnBorrow(true);
        JedisPool pool = new JedisPool(config, "192.168.218.129", 6379);
        Jedis jedis = pool.getResource();
        Map<String, String> person = jedis.hgetAll("person");
        System.out.println(person);
        jedis.close();
    }

    @Test
    public void test5() {
        Jedis jedis = new Jedis("192.168.218.129:6379|192.168.218.129:6380|192.168.218.129:6381|192.168.218.129:6382");
        Map<String, String> person = jedis.hgetAll("person");
        System.out.println(person);
        jedis.close();
    }
}
