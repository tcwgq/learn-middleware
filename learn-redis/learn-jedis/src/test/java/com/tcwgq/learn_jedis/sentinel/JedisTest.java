package com.tcwgq.learn_jedis.sentinel;

import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;

import java.util.HashSet;
import java.util.Set;

public class JedisTest {
    @Test
    public void testMasterSlave() {
        Set<String> set = new HashSet<>();
        set.add("192.168.218.129:26379");
        JedisSentinelPool pool = new JedisSentinelPool("mymaster", set, "123456");
        Jedis jedis = pool.getResource();
        String name = jedis.get("name");
        System.out.println(name);
        jedis.close();
    }

    @Test
    public void testMasterSlave1() {
    }

    @Test
    public void testMasterSlave2() {
    }

    @Test
    public void testMasterSlave3() {
    }

    @Test
    public void testMasterSlave4() {
    }
}
