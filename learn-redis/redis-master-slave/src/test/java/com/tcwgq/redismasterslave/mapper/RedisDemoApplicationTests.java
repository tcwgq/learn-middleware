package com.tcwgq.redismasterslave.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
class RedisDemoApplicationTests {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Test
    void writeTest() {
        redisTemplate.opsForValue().set("name", "Ross");
    }

    @Test
    void getTest() {
        /**
         * 停掉所有slave节点，测试读取报错
         * RedisDemoApplicationTests.getTest:21 » RedisSystem Redis exception; nested exception is io.lettuce.core.RedisException: Cannot determine a node to read (Known nodes: [RedisMasterReplicaNode [redisURI=redis://******@192.168.245.128, role=UPSTREAM]]) with setting io.lettuce.core.ReadFromImpl$ReadFromReplica@213835b6
         */
        Object name = redisTemplate.opsForValue().get("name");
        if (name != null) {
            System.out.println(name);
        }
    }

}