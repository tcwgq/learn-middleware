package com.tcwgq.rediscluster.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

@SpringBootTest
class RedisDemoApplicationTests {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    void writeTest() {
        redisTemplate.opsForValue().set("name", "Ross");
    }

    @Test
    void getTest() {
        Object name = redisTemplate.opsForValue().get("name");
        if (name != null) {
            System.out.println(name);
        }
    }

}