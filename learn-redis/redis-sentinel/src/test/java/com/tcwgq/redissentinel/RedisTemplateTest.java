package com.tcwgq.redissentinel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

/**
 * @author tcwgq
 * @since 2024/12/10 19:25
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class RedisTemplateTest {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    public void keysTest() {
        Set<String> keys = redisTemplate.keys("mutex_tokens:1716724780777660418:38274f0d274642309bb457523d045c66:*");
        System.out.println(keys);
    }

}
