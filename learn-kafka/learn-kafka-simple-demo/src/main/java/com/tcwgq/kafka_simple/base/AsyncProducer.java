package com.tcwgq.kafka_simple.base;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * 异步发送
 */
public class AsyncProducer {
    public static void main(String[] args) throws InterruptedException {
        Properties properties = new Properties();
        // kafka地址，多个地址用逗号分割
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.245.128:9092");
        // 设置重试次数
        properties.put(ProducerConfig.RETRIES_CONFIG, 10);
        // key序列化器
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        // value序列化器
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        try (KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(properties)) {
            for (int i = 0; i < 100; i++) {
                String msg = "Hello," + i;
                ProducerRecord<String, String> record = new ProducerRecord<>("heima", msg);
                kafkaProducer.send(record, (metadata, exception) -> {
                    if (exception == null) {
                        System.out.println(metadata.topic());
                        System.out.println(metadata.partition());
                        System.out.println(metadata.offset());
                        System.out.println("消息发送成功:" + msg);
                    } else {
                        System.err.println("消息发送异常");
                    }
                });
                TimeUnit.MILLISECONDS.sleep(500);
            }
        }
    }

}