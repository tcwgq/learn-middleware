package com.tcwgq.kafka_simple.base;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class Consumer {
    public static void main(String[] args) {
        Properties properties = new Properties();
        // kafka地址，多个地址用逗号分割
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.245.128:9092");
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        // 设置消费topic，不写会报错
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "group1");
        // 指定KafkaConsumer对应的客户端ID，默认为空，如果不设置KafkaConsumer会自动生成一个非空字符串
        properties.put(ConsumerConfig.CLIENT_ID_CONFIG, "consumer.client.id.demo");
        // 指定拦截器
        // properties.put(ConsumerConfig.INTERCEPTOR_CLASSES_CONFIG, ConsumerCustomInterceptor.class.getName());

        try (KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(properties)) {
            // 订阅消息
            kafkaConsumer.subscribe(Collections.singletonList("heima-par"));
            // 正则匹配
            // kafkaConsumer.subscribe(Pattern.compile("heima*"));
            // 指定订阅的分区
            // kafkaConsumer.assign(Arrays.asList(new TopicPartition("topic0701", 0)));

            while (true) {
                ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMillis(200));
                for (ConsumerRecord<String, String> record : records) {
                    System.out.printf("topic:%s,offset:%d,消息:%s%n", record.topic(), record.offset(), record.value());
                }
                // 同步提交
                // kafkaConsumer.commitSync();
            }
        }
    }

}