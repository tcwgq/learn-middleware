package com.tcwgq.kafka_simple.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @author tcwgq
 * @since 2022/7/20 20:19
 */
@Data
public class User implements Serializable {
    private Long id;
    private String username;
    private Integer age;

}
