package com.tcwgq.kafka_simple.base;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * 同步发送
 */
public class SyncProducer {
    public static void main(String[] args) throws InterruptedException {
        Properties properties = new Properties();
        // kafka地址，多个地址用逗号分割
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.245.128:9092");
        // 设置重试次数
        properties.put(ProducerConfig.RETRIES_CONFIG, 10);
        // key序列化器
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        // value序列化器
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        // 设置自定义分区器
        // properties.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, CustomPartitioner.class);
        // 自定义拦截器
        // properties.put(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG, CustomInterceptor.class.getName());
        // 设置acks，注意是个String类型
        // properties.put(ProducerConfig.ACKS_CONFIG, "0");

        try (KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(properties)) {
            for (int i = 0; i < 100; i++) {
                String msg = "Hello," + i;
                ProducerRecord<String, String> record = new ProducerRecord<>("heima", msg);
                Future<RecordMetadata> future = kafkaProducer.send(record);
                RecordMetadata metadata = future.get();
                System.out.println(metadata.topic());
                System.out.println(metadata.partition());
                System.out.println(metadata.offset());
                System.out.println("消息发送成功:" + msg);
                TimeUnit.MILLISECONDS.sleep(500);
            }
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

}