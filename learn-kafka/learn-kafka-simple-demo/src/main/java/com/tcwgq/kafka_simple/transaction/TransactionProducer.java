package com.tcwgq.kafka_simple.transaction;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.concurrent.Future;

/**
 * 同步发送
 */
@Slf4j
public class TransactionProducer {
    public static void main(String[] args) {
        Properties properties = new Properties();
        // kafka地址，多个地址用逗号分割
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.245.128:9092");
        // 设置重试次数
        // properties.put(ProducerConfig.RETRIES_CONFIG, 10);
        // key序列化器
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        // value序列化器
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        // 开启幂等性，默认开启
        properties.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
        // 开启事务
        properties.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, "transactionId");

        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(properties);
        // 初始化事务
        kafkaProducer.initTransactions();
        // 开启事务
        kafkaProducer.beginTransaction();
        try {
            // 处理业务逻辑并创建ProducerRecord
            ProducerRecord<String, String> record1 = new ProducerRecord<>("heima-pai", "msg1");
            Future<RecordMetadata> future = kafkaProducer.send(record1);
            RecordMetadata recordMetadata = future.get();
            // 模拟事务回滚案例
            System.out.println(1 / 0);
            ProducerRecord<String, String> record2 = new ProducerRecord<>("heima-pai", "msg2");
            kafkaProducer.send(record2);
            ProducerRecord<String, String> record3 = new ProducerRecord<>("heima-pai", "msg3");
            kafkaProducer.send(record3);
            // 提交事务
            kafkaProducer.commitTransaction();
        } catch (Exception e) {
            log.error("发送消息过程出现异常，message={}", e.getMessage(), e);
            kafkaProducer.abortTransaction();
        }
        kafkaProducer.close();
    }

}