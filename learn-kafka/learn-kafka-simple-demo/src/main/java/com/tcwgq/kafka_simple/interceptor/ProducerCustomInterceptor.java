package com.tcwgq.kafka_simple.interceptor;

import org.apache.kafka.clients.producer.ProducerInterceptor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Map;

/**
 * @author tcwgq
 * @since 2022/7/20 20:32
 */
public class ProducerCustomInterceptor implements ProducerInterceptor<String, String> {
    @Override
    public ProducerRecord<String, String> onSend(ProducerRecord<String, String> record) {
        return new ProducerRecord<>(record.topic(), "test_" + record.value());
    }

    @Override
    public void onAcknowledgement(RecordMetadata metadata, Exception exception) {
        System.out.println("onAcknowledgement");
    }

    @Override
    public void close() {
        System.out.println("拦截器关闭");
    }

    @Override
    public void configure(Map<String, ?> configs) {

    }
}
