package com.tcwgq.kafka_springboot.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author tcwgq
 * @since 2022/7/21 18:31
 */
@Slf4j
@Component
public class ConsumerListener {
    @KafkaListener(id = "client1", topics = "heima-par", groupId = "group1")
    public void onMessage(String message) {
        log.info("kafka接收到消息 message={}", message);
    }

}
