package com.tcwgq.kafka_springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tcwgq
 * @since 2022/7/21 18:25
 */
@RestController
@RequestMapping
public class SendController {
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @GetMapping("/send/{message}")
    public String send(@PathVariable("message") String message) {
        kafkaTemplate.send("heima-par", message);
        return "success";
    }

    /**
     * 事务实现方式一
     *
     * @param message
     * @return
     */
    @GetMapping("/tx/{message}")
    public String tx(@PathVariable("message") String message) {
        kafkaTemplate.executeInTransaction(operations -> {
            operations.send("heima-par", message + 1);
            operations.send("heima-par", message + 2);
            if (message.equals("error")) {
                throw new RuntimeException("消息错误");
            }
            operations.send("heima-par", message + 3);
            return "事务消息发送成功";
        });
        return "success";
    }

    /**
     * 事务实现方式二
     *
     * @param message
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    @GetMapping("/tx1/{message}")
    public String tx1(@PathVariable("message") String message) {
        kafkaTemplate.send("heima-par", message + 1);
        kafkaTemplate.send("heima-par", message + 2);
        if (message.equals("error")) {
            throw new RuntimeException("消息错误");
        }
        kafkaTemplate.send("heima-par", message + 3);
        return "success";
    }

}
