package com.tcwgq.es_springboot.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * 注意父pom中对项目配置文件的读取，spring默认提供了对yml文件的支持
 *
 * @author tcwgq
 * @since 2022/7/12 10:24
 */
@Service
public class UserService {
    @Value("${spring.data.elasticsearch.cluster-nodes}")
    private String value;

    @PostConstruct
    public void post() {
        System.out.println(value);
    }

}
