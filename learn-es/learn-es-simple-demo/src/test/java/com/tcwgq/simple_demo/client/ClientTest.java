package com.tcwgq.simple_demo.client;

import org.elasticsearch.action.bulk.*;
import org.elasticsearch.action.bulk.BulkProcessor.Listener;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.node.DiscoveryNode;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.junit.Before;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Elasticsearch为Java用户提供了两种内置客户端：
 * 节点客户端(node client)：
 * 节点客户端以无数据节点(none data node)身份加入集群，
 * 换言之，它自己不存储任何数据，但是它知道数据在集群中的具体位置，并且能够直接转发请求到对应的节点上。
 * <p>
 * <p>
 * 传输客户端(Transport client)：
 * 这个更轻量的传输客户端能够发送请求到远程集群。它自己不加入集群，只是简单转发请求给集群中的节点。
 * 两个Java客户端都通过9300端口与集群交互，使用Elasticsearch传输协议(Elasticsearch Transport
 * Protocol)。集群中的节点之间也通过9300端口进行通信。如果此端口未开放，你的节点将不能组成集群。+
 * <p>
 * <p>
 * Java客户端所在的Elasticsearch版本必须与集群中其他节点一致，否则，它们可能互相无法识别。
 *
 * @author tcwgq
 * @since 2018年1月23日下午3:18:23
 */
public class ClientTest {
    TransportClient client;

    @Before
    public void setup() throws UnknownHostException {
        Settings settings = Settings.builder()
                // 设置ES实例的名称，需和服务端集群名称一致
                .put("cluster.name", "my-es-cluster")
                // 自动嗅探整个集群的状态，把集群中其他ES节点的ip添加到本地的客户端列表中
                .put("client.transport.sniff", true)
                .build();
        // 初始化client较老版本发生了变化，此方法有几个重载方法，初始化插件等。
        client = new PreBuiltTransportClient(settings);
        // 此步骤添加IP，至少一个，其实一个就够了，因为添加了自动嗅探配置
        client.addTransportAddress(new TransportAddress(InetAddress.getByName("192.168.245.128"), 9300));
        System.out.println(client.toString());
    }

    /**
     * 查看集群信息
     */
    @Test
    public void getClusterInfo() {
        List<DiscoveryNode> nodes = client.connectedNodes();
        for (DiscoveryNode node : nodes) {
            System.out.println(node.getHostAddress());
        }
    }

    /**
     * The following example indexes a JSON document into an index called
     * twitter, under a type called tweet, with id valued 1
     */
    @Test
    public void put() throws Exception {
        XContentBuilder source = createJson();
        // 存json入索引中
        IndexResponse response = client.prepareIndex("twitter", "tweet", "1").setSource(source).get();
        // Index name
        String index = response.getIndex();
        // Type name
        String type = response.getType();
        // Document ID (generated or not)
        String id = response.getId();
        // Version (if it's the first time you index this document, you will
        // get: 1)
        long version = response.getVersion();
        // status has stored current instance statement.
        RestStatus status = response.status();
        System.out.println(index + " : " + type + ": " + id + ": " + version + ": " + status);
    }

    /**
     * 使用es的帮助类
     */
    public XContentBuilder createJson() throws Exception {
        // 创建json对象, 其中一个创建json的方式
        return XContentFactory.jsonBuilder().//
                startObject().//
                field("user", "kimchy").//
                field("postDate", new Date()).//
                field("message", "trying to out ElasticSearch").//
                endObject();
    }

    /**
     * get API 获取指定文档信息
     */
    @Test
    public void get() {
        GetResponse response = client.prepareGet("twitter", "tweet", "1").get();
        System.out.println(response.getSourceAsString());
    }

    @Test
    public void query() throws Exception {
        // term查询
        // QueryBuilder queryBuilder = QueryBuilders.termQuery("age", 50) ;
        // range查询
        QueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("id").lt(2);
        SearchResponse searchResponse = client.prepareSearch("twitter").setTypes("tweet").setQuery(rangeQueryBuilder).addSort("id", SortOrder.DESC).setSize(20).execute().actionGet();
        SearchHits hits = searchResponse.getHits();
        System.out.println("查到记录数：" + hits.getTotalHits());
        SearchHit[] searchHists = hits.getHits();
        if (searchHists.length > 0) {
            for (SearchHit hit : searchHists) {
                System.out.format(hit.getSourceAsString());
            }
        }
    }

    /**
     * 测试 delete api
     */
    @Test
    public void delete() {
        DeleteResponse response = client.prepareDelete("twitter", "tweet", "1").get();
        String index = response.getIndex();
        String type = response.getType();
        String id = response.getId();
        long version = response.getVersion();
        System.out.println(index + " : " + type + ": " + id + ": " + version);
    }

    /**
     * 测试更新 update API 使用 updateRequest 对象
     */
    @Test
    public void update() throws Exception {
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.index("twitter");
        updateRequest.type("tweet");
        updateRequest.id("1");
        updateRequest.doc(XContentFactory.jsonBuilder().startObject()
                // 对没有的字段添加, 对已有的字段替换
                .field("gender", "male").field("message", "hello").endObject());
        UpdateResponse response = client.update(updateRequest).get();

        // 打印
        String index = response.getIndex();
        String type = response.getType();
        String id = response.getId();
        long version = response.getVersion();
        System.out.println(index + " : " + type + ": " + id + ": " + version);
    }

    /**
     * 测试update api, 使用client
     */
    @Test
    public void testUpdate2() throws Exception {
        // 使用Script对象进行更新
        // UpdateResponse response = client.prepareUpdate("twitter", "tweet",
        // "1")
        // .setScript(new Script("hits._source.gender = \"male\""))
        // .get();

        // 使用XContFactory.jsonBuilder() 进行更新
        // UpdateResponse response = client.prepareUpdate("twitter", "tweet",
        // "1")
        // .setDoc(XContentFactory.jsonBuilder()
        // .startObject()
        // .field("gender", "malelelele")
        // .endObject()).get();

        // 使用updateRequest对象及script
        // UpdateRequest updateRequest = new UpdateRequest("twitter", "tweet",
        // "1")
        // .script(new Script("ctx._source.gender=\"male\""));
        // UpdateResponse response = client.update(updateRequest).get();

        // 使用updateRequest对象及documents进行更新
        UpdateResponse response = client.update(new UpdateRequest("twitter", "tweet", "1")
                .doc(XContentFactory.jsonBuilder().startObject().field("gender", "male").endObject())).get();
        System.out.println(response.getIndex());
    }

    /**
     * 测试update 使用updateRequest
     */
    @Test
    public void testUpdate3() throws Exception {
        UpdateRequest updateRequest = new UpdateRequest("twitter", "tweet", "1").script(new Script("ctx._source.gender=\"male\""));
        UpdateResponse response = client.update(updateRequest).get();
        System.out.println(response.toString());
    }

    /**
     * 测试upsert方法
     */
    @Test
    public void testUpsert() throws Exception {
        // 设置查询条件, 查找不到则添加生效
        IndexRequest indexRequest = new IndexRequest("twitter", "tweet", "1").source(XContentFactory.jsonBuilder().startObject().field("name", "qergef").field("gender", "malfdsae").endObject());
        // 设置更新, 查找到更新下面的设置
        UpdateRequest upsert = new UpdateRequest("twitter", "tweet", "1").doc(XContentFactory.jsonBuilder().startObject().field("user", "wenbronk").endObject()).upsert(indexRequest);
        client.update(upsert).get();
    }

    /**
     * 测试multi get api 从不同的index, type, 和id中获取
     */
    @Test
    public void testMultiGet() {
        MultiGetResponse multiGetResponse = client.prepareMultiGet().add("twitter", "tweet", "1").add("twitter", "tweet", "2", "3", "4").add("anothoer", "type", "foo").get();
        for (MultiGetItemResponse itemResponse : multiGetResponse) {
            GetResponse response = itemResponse.getResponse();
            if (response.isExists()) {
                String sourceAsString = response.getSourceAsString();
                System.out.println(sourceAsString);
            }
        }
    }

    /**
     * bulk 批量执行 一次查询可以update 或 delete多个document
     */
    @Test
    public void testBulk() throws Exception {
        BulkRequestBuilder bulkRequest = client.prepareBulk();
        bulkRequest.add(client.prepareIndex("twitter", "tweet", "1").setSource(XContentFactory.jsonBuilder().startObject().field("user", "kimchy").field("postDate", new Date()).field("message", "trying out Elasticsearch").endObject()));
        bulkRequest.add(client.prepareIndex("twitter", "tweet", "2").setSource(XContentFactory.jsonBuilder().startObject().field("user", "kimchy").field("postDate", new Date()).field("message", "another post").endObject()));
        BulkResponse response = bulkRequest.get();
        System.out.println(response.status());
    }

    /**
     * 使用bulk processor
     */
    @Test
    public void testBulkProcessor() throws Exception {
        // 创建BulkProcessor对象
        BulkProcessor bulkProcessor = BulkProcessor.builder(client, new Listener() {
                    public void beforeBulk(long paramLong, BulkRequest paramBulkRequest) {
                        // TODO Auto-generated method stub
                    }

                    // 执行出错时执行
                    public void afterBulk(long paramLong, BulkRequest paramBulkRequest, Throwable paramThrowable) {
                        // TODO Auto-generated method stub
                    }

                    public void afterBulk(long paramLong, BulkRequest paramBulkRequest, BulkResponse paramBulkResponse) {
                        // TODO Auto-generated method stub
                    }
                })
                // 1w次请求执行一次bulk
                .setBulkActions(10000)
                // 1gb的数据刷新一次bulk
                .setBulkSize(new ByteSizeValue(1, ByteSizeUnit.GB))
                // 固定5s必须刷新一次
                .setFlushInterval(TimeValue.timeValueSeconds(5))
                // 并发请求数量, 0不并发, 1并发允许执行
                .setConcurrentRequests(1)
                // 设置退避, 100ms后执行, 最大请求3次
                .setBackoffPolicy(BackoffPolicy.exponentialBackoff(TimeValue.timeValueMillis(100), 3)).build();

        // 添加单次请求
        bulkProcessor.add(new IndexRequest("twitter", "tweet", "1"));
        bulkProcessor.add(new DeleteRequest("twitter", "tweet", "2"));

        // 关闭
        bulkProcessor.awaitClose(10, TimeUnit.MINUTES);
        // 或者
        bulkProcessor.close();
    }

}
