package com.tcwgq;

import com.mongodb.*;
import org.junit.Test;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author iyb-wangguangqiang 2019/5/20 15:24
 */
public class MongodbTest {
    private final String username = "root";
    private final String password = "root";
    private final String database = "admin";
    private final String host = "localhost";
    private final int port = 27017;

    @Test
    public void getConn() throws UnknownHostException {
        List<ServerAddress> serverList = new ArrayList<>();
        ServerAddress serverAddress = new ServerAddress(host, port);
        serverList.add(serverAddress);

        List<MongoCredential> credentialList = new ArrayList<>();
        MongoCredential credential = MongoCredential.createCredential(username, database, password.toCharArray());
        credentialList.add(credential);
        MongoClient client = new MongoClient(serverList, credentialList);
        List<String> databaseNames = client.getDatabaseNames();
        System.out.println(databaseNames);
    }

    @Test
    public void getConn1() throws UnknownHostException {
        // mongodb//username:password@url:port/db
        String sURI = String.format("mongodb://%s:%s@%s:%d/%s", username, password, host, port, database);
        MongoClientURI uri = new MongoClientURI(sURI);
        MongoClient client = new MongoClient(uri);
        List<String> databaseNames = client.getDatabaseNames();
        System.out.println(databaseNames);
    }

    @Test
    public void getDB() throws UnknownHostException {
        String sURI = String.format("mongodb://%s:%s@%s:%d/%s", username, password, host, port, database);
        MongoClientURI uri = new MongoClientURI(sURI);
        MongoClient client = new MongoClient(uri);
        DB test = client.getDB("test");
        Set<String> collectionNames = test.getCollectionNames();
        System.out.println(collectionNames);
    }

    @Test
    public void getDB1() throws UnknownHostException {
        String sURI = String.format("mongodb://%s:%s@%s:%d/%s", username, password, host, port, database);
        MongoClientURI uri = new MongoClientURI(sURI);
        MongoClient client = new MongoClient(uri);
        DB test = client.getDB("test");
        DBCollection animal = test.getCollection("animal");
        DBObject object = new BasicDBObject("name", "dog");
        WriteResult insert = animal.insert(object);
        System.out.println(insert);
    }

    @Test
    public void insertOne() throws UnknownHostException {
        String sURI = String.format("mongodb://%s:%s@%s:%d/%s", username, password, host, port, database);
        MongoClientURI uri = new MongoClientURI(sURI);
        MongoClient client = new MongoClient(uri);
        DB test = client.getDB("test");
        DBCollection animal = test.getCollection("animal");
        DBObject object = new BasicDBObject("name", "dog");
        WriteResult insert = animal.insert(object);
        System.out.println(insert);
    }

    @Test
    public void insertBatch() throws UnknownHostException {
        String sURI = String.format("mongodb://%s:%s@%s:%d/%s", username, password, host, port, database);
        MongoClientURI uri = new MongoClientURI(sURI);
        MongoClient client = new MongoClient(uri);
        DB test = client.getDB("test");
        DBCollection animal = test.getCollection("animal");
        List<DBObject> objectList = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            DBObject object = new BasicDBObject("name", "dog" + i);
            objectList.add(object);
        }
        WriteResult insert = animal.insert(objectList);
        System.out.println(insert);
    }

    @Test
    public void delete() throws UnknownHostException {
        String sURI = String.format("mongodb://%s:%s@%s:%d/%s", username, password, host, port, database);
        MongoClientURI uri = new MongoClientURI(sURI);
        MongoClient client = new MongoClient(uri);
        DB test = client.getDB("test");
        DBCollection animal = test.getCollection("animal");
        DBObject object = new BasicDBObject("name", "dog");
        WriteResult remove = animal.remove(object);
        System.out.println(remove);
    }

    @Test
    public void update() throws UnknownHostException {
        String sURI = String.format("mongodb://%s:%s@%s:%d/%s", username, password, host, port, database);
        MongoClientURI uri = new MongoClientURI(sURI);
        MongoClient client = new MongoClient(uri);
        DB test = client.getDB("test");
        DBCollection animal = test.getCollection("animal");
        DBObject old = new BasicDBObject("name", "cat");
        DBObject update = new BasicDBObject("name", "dog");
        WriteResult result = animal.update(old, update);
        System.out.println(result);
    }

    @Test
    public void findOne() throws UnknownHostException {
        String sURI = String.format("mongodb://%s:%s@%s:%d/%s", username, password, host, port, database);
        MongoClientURI uri = new MongoClientURI(sURI);
        MongoClient client = new MongoClient(uri);
        DB test = client.getDB("test");
        DBCollection animal = test.getCollection("animal");
        // 返回文档中的第一个
        DBObject one = animal.findOne();
        System.out.println(one);
        DBObject one1 = animal.findOne(new BasicDBObject("name", "dog"));
        System.out.println(one1);
    }

    @Test
    public void findInsert() throws UnknownHostException {
        String sURI = String.format("mongodb://%s:%s@%s:%d/%s", username, password, host, port, database);
        MongoClientURI uri = new MongoClientURI(sURI);
        MongoClient client = new MongoClient(uri);
        DB test = client.getDB("test");
        // db.createCollection("app", {capped: <Boolean >, autoIndexId: <Boolean >, size: <number >, max < number >} )
//        DBObject option = new BasicDBObject("capped", true).append("autoIndexId", true).append("size", 10000).append("max", 1000000);
//        DBCollection app = test.createCollection("app", option);
        DBCollection app = test.getCollection("app");
        System.out.println(app);
        List<DBObject> objectList = new ArrayList<>();
        for (int i = 1; i <= 1000; i++) {
            DBObject object = new BasicDBObject("id", i).append("name", "zhangSan" + 1).append("age", i);
            objectList.add(object);
        }
        WriteResult insert = app.insert(objectList);
        System.out.println(insert);
    }

    @Test
    public void find() throws UnknownHostException {
        String sURI = String.format("mongodb://%s:%s@%s:%d/%s", username, password, host, port, database);
        MongoClientURI uri = new MongoClientURI(sURI);
        MongoClient client = new MongoClient(uri);
        DB test = client.getDB("test");
        DBCollection app = test.getCollection("app");
        DBObject query = new BasicDBObject("age", new BasicDBObject("$gte", 900));
        DBCursor dbObjects = app.find(query);
        while (dbObjects.hasNext()) {
            System.out.println(dbObjects.next());
        }
    }

    @Test
    public void find1() throws UnknownHostException {
        String sURI = String.format("mongodb://%s:%s@%s:%d/%s", username, password, host, port, database);
        MongoClientURI uri = new MongoClientURI(sURI);
        MongoClient client = new MongoClient(uri);
        DB test = client.getDB("test");
        DBCollection app = test.getCollection("app");
        DBObject query = new BasicDBObject("age", new BasicDBObject("$gte", 900));
        DBCursor dbObjects = app.find(query);
        while (dbObjects.hasNext()) {
            System.out.println(dbObjects.next());
        }
    }

    @Test
    public void find2() throws UnknownHostException {
        String sURI = String.format("mongodb://%s:%s@%s:%d/%s", username, password, host, port, database);
        MongoClientURI uri = new MongoClientURI(sURI);
        MongoClient client = new MongoClient(uri);
        DB test = client.getDB("test");
        DBCollection app = test.getCollection("app");
        DBObject query = new BasicDBObject("age", new BasicDBObject("$gte", 900));
        DBCursor dbObjects = app.find(query);
        while (dbObjects.hasNext()) {
            System.out.println(dbObjects.next());
        }
    }


}