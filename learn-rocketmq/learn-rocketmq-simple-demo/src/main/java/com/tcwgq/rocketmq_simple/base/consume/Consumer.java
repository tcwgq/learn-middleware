package com.tcwgq.rocketmq_simple.base.consume;

import org.apache.rocketmq.client.consumer.DefaultMQPullConsumer;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author tcwgq
 * @since 2022/7/15 20:09
 */
public class Consumer {
    public static void main(String[] args) throws MQClientException, InterruptedException {
        push();
    }

    public static void push() throws MQClientException, InterruptedException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("group1");
        consumer.setNamesrvAddr("192.168.245.128:9876;192.168.245.129:9876");
        consumer.subscribe("BaseTopic", "*");
        // 默认集群模式消费
        consumer.setMessageModel(MessageModel.BROADCASTING);
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                List<String> collect = msgs.stream().map(messageExt -> new String(messageExt.getBody())).collect(Collectors.toList());
                System.out.println(collect);
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        consumer.start();
    }

    public static void pull() throws MQClientException, InterruptedException {
        DefaultMQPullConsumer consumer = new DefaultMQPullConsumer("group1");
        consumer.setNamesrvAddr("192.168.245.128:9876;192.168.245.129:9876");
        consumer.start();
        TimeUnit.SECONDS.sleep(1000);
    }

}
