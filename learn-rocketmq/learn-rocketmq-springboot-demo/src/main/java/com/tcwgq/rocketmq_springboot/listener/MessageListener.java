package com.tcwgq.rocketmq_springboot.listener;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @author tcwgq
 * @since 2022/7/16 17:55
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = "springboot-topic", consumerGroup = "my-consumer")
public class MessageListener implements RocketMQListener<String> {
    @Override
    public void onMessage(String message) {
        log.info("receive message: {}", message);
    }

}
